import React, { Component } from 'react';

export default class DisplayTask extends Component {
  render() {
    return (
      <div>
        <ul>
            {this.props.items.map(item => (
                <li key={item.id}>{item.text}</li>
            ))}
        </ul>
      </div>
    )
  }
};
