import './App.css';
import Increment from './Increment.js';
import TodoList from './TodoList.js';
import React from 'react';

const App = () => {
    return (
      <div className="App">
        <Increment />
        <TodoList />
      </div>
    );
}

export default App;