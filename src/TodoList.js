import React, { Component } from 'react';
import DisplayTask from './DisplayTask.js';

export default class TodoList extends Component {

    constructor(props){
        super(props);
        this.state = {
            items: [],
            count: 0,
            text: ""
        };
        this.keepTask = this.keepTask.bind(this);
        this.ajouter = this.ajouter.bind(this);
    }

    keepTask(e) {
        this.setState({text: e.target.value});
    }
    ajouter(e) {
        this.setState({count: this.state.count + 1})
        e.preventDefault();
        if (!this.state.text.length) {
            return;
        }
        const newItem = {
            text: this.state.text,
            id: Date.now()
        };
        this.setState(prevState => ({
            items: prevState.items.concat(newItem),
            text: ''
        }));
        
    }
    
    render() {
        return (
        <div>
            <h1>TodoList</h1>
            <DisplayTask items={this.state.items}/>
            <p>Entrez une tache :</p>
            <input value={this.state.text} onChange={this.keepTask} type="text"/>
            <button onClick={this.ajouter}>Add #{this.state.items.length +1}</button>
        </div>
        )
    }
};
