import React from 'react';

export default class Increment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            text: ""
        };
        this.incrementer = this.incrementer.bind(this);
        this.decrementer = this.decrementer.bind(this);
        this.changeTexte = this.changeTexte.bind(this);
    }

    incrementer() {
        if (this.state.count >= 0)
            this.setState({ count: this.state.count + 1 });
    }

    decrementer() {
        if (this.state.count > 0)
            this.setState({ count: this.state.count - 1 });
    }

    changeTexte(e) {
        this.setState({ text: e.target.value });
    }

    render() {
        return (
            <div>
                <p>Qui etes vous ?</p>
                <input onChange={this.changeTexte} type="text" />
                <p>{this.state.text}, Vous avez {this.state.count} amis.</p>
                <button onClick={this.incrementer}>plus d'amis</button>
                <button onClick={this.decrementer}>moins d'amis</button><br/><br/>
            </div>
        );
    }
}